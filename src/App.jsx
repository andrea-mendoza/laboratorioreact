import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Components/Home';
import Register from './Components/Register';
import Login from './Components/Login';
import CustomNavbar from './Components/CustomNavbar';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home}></Route>
          <Route path="/login" component={Login}></Route>
          <Route exact path="/register" component={Register}></Route>
          {/* <CustomNavbar/> */}
        </div>
      </Router>
    );
  }
}

export default App;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, Image } from 'react-bootstrap';
import '../App.css';
import './Login.css';

class Login extends Component {
    constructor(){
        super();
        this.state = { 
                validated: false,
                email:"",
                password:"" 
                };
    }

    handleEmailChange = evt => {
        this.setState({ email: evt.target.value });
    };
    
    handlePasswordChange = evt => {
        this.setState({ password: evt.target.value });
    };

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        else{
            this.saveData();
        }
        this.setState({ validated: true });
    }


    saveData(){
        var email =document.getElementById("emailForm").value;
        var password = document.getElementById("passwordForm").value;
        console.log("Email: " + email);
        console.log("Password: " + password);
        this.GenerarJSONFile(email,password);
    }

    GenerarJSONFile(email,password){
        var jsonObj = {Email: email,
                       Password: password
                    };
        var fileText = new Blob([JSON.stringify(jsonObj)],{type:"application/json"});
        var textuRL = window.URL.createObjectURL(fileText);
    
        var downloadLink = document.createElement("a");
        downloadLink.download = "loginData.json";
        downloadLink.innerHTML = "";
        downloadLink.href = textuRL;
    
        document.body.appendChild(downloadLink);
        downloadLink.click();
    }

    canBeSubmitted() {
        const { validated, email, password } = this.state;
        return email.length > 0 && password.length > 0;
    }

    render() {
        const isEnabled = this.canBeSubmitted();
        const { validated} = this.state;
        return (
            <div className="background-box">
            <div className="formBox">
                <div className="headerLoginStyle">
                    <Image src="assets/icon.png" className="imageStyle"/>
                    <h1>
                        Sign In
                    </h1>
                </div>
                <div className="formBoxColor">
                    <Form noValidate
                        validated={validated}
                        onSubmit={e => this.handleSubmit(e)}>
                        <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control required type="email" id="emailForm" placeholder="Enter email" value={this.state.email} onChange={this.handleEmailChange}/>
                            <Form.Control.Feedback type="invalid"> Your email is missing or Invald</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" type="password" id="passwordForm" value={this.state.password} placeholder="Password"  onChange={this.handlePasswordChange}/>
                            <Form.Control.Feedback type="invalid"> Minimum length is 8 characters and at least 1 number </Form.Control.Feedback>
                        </Form.Group>
                            {/* <Button variant="primary"> Submit </Button> */}
                            <Button disabled={!isEnabled} variant="primary" type="submit"> Submit </Button>
                    </Form> 
             </div>
             </div>
            </div>
        );
    }
}

export default Login;
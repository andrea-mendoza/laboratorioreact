import React, { Component } from 'react';
import { Jumbotron, Container, Form, Col, Button } from 'react-bootstrap';
import './Register.css';

class Register extends Component {
    constructor(){
        super();
        this.state = { validated: false };
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        else{
            this.saveData();
        }
        this.setState({ validated: true });
    }

    saveData(){
        var name = document.getElementById("name").value;
        var lastname = document.getElementById("lastname").value;
        var email =document.getElementById("emailForm").value;
        var password = document.getElementById("passwordForm").value;
        var city = document.getElementById("city").value;
        var state = document.getElementById("state").value;
        var address = document.getElementById("address").value;
        console.log("Nombre: " + name);
        console.log("Apellido: " + lastname);
        console.log("Email: " + email);
        console.log("Password: " + password);
        console.log("Direccion: " + address);
        console.log("Departamento: " + state);
        console.log("Ciudad: " + city);

        this.GenerarJSONFile(name, lastname,address,city,state, email,password);
    }

    GenerarJSONFile(name,lastname, address, city, state, email, password){
        var jsonObj = {Name: name,
                       Lastname: lastname,
                       Address: {address, city, state},
                       Email: email,
                       Password: password
                    };
        var fileText = new Blob([JSON.stringify(jsonObj)],{type:"application/json"});
        var textuRL = window.URL.createObjectURL(fileText);
    
        var downloadLink = document.createElement("a");
        downloadLink.download = "registrationData.json";
        downloadLink.innerHTML = "";
        downloadLink.href = textuRL;
    
        document.body.appendChild(downloadLink);
        downloadLink.click();
    }

    render() {
        const { validated } = this.state;
        return (
            <div className="formBoxRegister background-box-register">
                <h1 className="h1Style">Registration</h1>
                <div className="registerFormBoxColor">
                <Form noValidate
                        validated={validated}
                        onSubmit={e => this.handleSubmit(e)}>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>Name</Form.Label>
                        <Form.Control required type="text" id="name" placeholder="Enter Name" />
                        </Form.Group>

                        <Form.Group as={Col}>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control required id="lastname" type="text" id="lastname" placeholder="Last Name" />
                        </Form.Group>
                    </Form.Row>
                    <Form.Group>
                        <Form.Label>Address</Form.Label>
                        <Form.Control required id="address" placeholder="Where do you live?" />
                    </Form.Group>
                    <Form.Row>
                        <Form.Group as={Col}>
                        <Form.Label>City</Form.Label>
                        <Form.Control required  id="city" type="text" placeholder="Enter City" />
                        </Form.Group>
                        <Form.Group as={Col} >
                            <Form.Label>State</Form.Label>
                            <Form.Control id= "state" as="select">
                                <option>Cochabamba</option>
                                <option>La Paz</option>
                                <option>Santa Cruz</option>
                                <option>Tarija</option>
                                <option>Potosi</option>
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>
                    <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control required type="email" id="emailForm" placeholder="Enter email" />
                            <Form.Control.Feedback type="invalid"> Your email is missing or Invald</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" type="password" id="passwordForm" placeholder="Password" />
                            <Form.Control.Feedback type="invalid"> Minimum length is 8 characters and at least 1 number </Form.Control.Feedback>
                        </Form.Group>
                        <Button variant="primary" type ="submit"> Submit </Button>
                        {/* <Button variant="primary" onClick={() => this.saveData()}> Submit </Button> */}
                </Form>
                </div>
            </div>
        );
    }
}

export default Register;
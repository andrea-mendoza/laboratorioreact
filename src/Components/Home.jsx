import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Jumbotron, Card,Container, Row, Col, Image, Button } from 'react-bootstrap';
import './Home.css';
import CustomNavbar from './CustomNavbar';
class Home extends Component {
    render() {
        return (
            <div className="background-style">
            <CustomNavbar/>
            <br/>
                <div className="padding-j">
                    <Jumbotron >
                        <div className="message-box">
                            <div className="message-font">
                                <b><h2>Centro <br/>de<br/> Adopción</h2></b>
                            </div>
                            <p>Ayuda a cambiar sus vidas </p>                            
                        </div>
                    </Jumbotron>
                </div>
                <Container>
                    <Row className="text-center">
                        <Col md="auto" className="pet-wrapper">
                            <Image src="assets/perrito-3.jpg" fluid roundedCircle className="dog-pic"/>
                            <h2>Ayuda</h2>
                            <p>Siempre es buena una donacion. A porta con un granito de arena</p>
                            <Link to="/">
                                <Button variant="info">Dona Ahora</Button>
                            </Link>
                        </Col>
                        <Col className="pet-wrapper">
                            <Image src="assets/gatito.jpg" roundedCircle className="dog-pic"/>
                            <h2>Educa</h2>
                            <p>Ayudemos a crear una mejor sociedad para nuestros amigos peludos</p>
                            <Link to="/">
                                <Button variant="info">Puntos de Enseñanza</Button>
                            </Link>
                        </Col>
                        <Col  className="pet-wrapper">
                            <Image src="assets/perrito-1.jpg" roundedCircle className="dog-pic"/>
                            <h2>Adopta</h2>
                            <p>Cambia la vida de una mascota y deja que este pueda cambie la tuya</p>
                            <Link to="/">
                                <Button variant="info">Encuentra un mejor amigo</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
                <br/>

                <Card className="text-center">
                    <Card.Footer>
                        <p>Organizacion sin fines de lucro</p>
                    </Card.Footer>
                </Card>
            </div>
        );
    }
}

export default Home;
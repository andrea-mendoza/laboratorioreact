import React, { Component } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import "./CustomNavbar.css";

class CustomNavbar extends Component {
    render() {
        return (
            <Navbar default collapseOnSelect sticky="top">
                <Navbar.Brand>
                    <Link className="linkColor" to="/">Patitas en Casa</Link>
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
                <Nav className="justify-content-end">
                    <Nav.Link href="/" to="/"> Inicio </Nav.Link>
                    <Nav.Link href="/" to="/"> En Adopcion </Nav.Link>
                    <Nav.Link href="/" to="/"> Tienda </Nav.Link>
                    <Nav.Link href="/" to="/"> Sobre Nosotros </Nav.Link>
                    <Nav.Link href="/login" to="/login"> Login </Nav.Link>
                    <Nav.Link href="/register" to="/register"> Register </Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default CustomNavbar;